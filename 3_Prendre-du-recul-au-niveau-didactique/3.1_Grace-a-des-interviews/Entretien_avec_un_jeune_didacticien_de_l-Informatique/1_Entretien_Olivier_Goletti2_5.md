# Entretien avec Olivier Goletti 2/5 : de la didactique de l'informatique.

Olivier Goletti est un jeune chercheur en didactique de l'informatique. Ces entretiens permettent d'éclairer ce que recouvre
ce nouveau champs disciplinaire.

## Entretien avec Olivier Goletti, Doctorant en didactique de l'informatique à l'UCLouvain

**Sommaire des 5 vidéos**

* 1/5 [Olivier Goletti, qui es-tu ?](./1_Entretien_Olivier_Goletti1_5.md)
* **2/5 Didactique de l'informatique ?**
* 3/5 [Quelques exemples\.](./1_Entretien_Olivier_Goletti3_5.md)
* 4/5 [Pensée informatique\.](./1_Entretien_Olivier_Goletti4_5.md)
* 5/5 [Quelques conseils plus pratiques\.](./1_Entretien_Olivier_Goletti5_5.md)


## 2/5 Didactique de l'informatique ?

Dans cette vidéo, Olivier Goletti, définit le terme de didactique et sa déclinaison plus spécifique à la discipline de l'informatique, en nous présentant les grandes questions auxquelles la didactique tente de répondre..

[![Entretien Olivier Goletti 2/5](https://mooc-nsi-snt.gitlab.io/portail/assets/Diapo-itw-OG1.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-OG-2.mp4)

La [didactique](https://fr.wikipedia.org/wiki/Didactique) est un terme qui décrit l'étude de la relation entre étudiant·e·s, enseignant·e·s et un savoir, la didactique de l'informatique concerne donc une discipline en particulier.
Dans la littérature anglo-saxonne, on parle de ”Computing Education Research (CER)” ou encore de ”Computer Science Education (CSEd) research”, donc de l'étude de la façon dont les gens apprennent et enseignent l'informatique, en reliant les idées fondamentales qui couvrent tout apprentissage humain (de la recherche en éducation, à la psychologie de l'éducation et aux sciences de l'apprentissage) aux idées appliquées spécifiquement à l'apprentissage de compétences informatiques.

_
The study of how people learn and teach computing...
Bridging foundational ideas that span any human learning (from Education research, Educational psychology and Learning sciences) to applied ideas specific to the learning of specific ideas and skills in CS.
_

[Amy J. Ko](https://faculty.washington.edu/ajko/cer)

Voici quelques grandes questions qui sont posées, d'une part en lien avec comment les personnes apprennent, que veut dire comprendre un concept, comment peut-on l'acquérir, et au-delà l'utiliser, comment peut-on alors vérifier cette acquisition, il y a aussi toutes les questions liées aux personnes apprenantes, on se pose des questions sur l'efficacité des méthodes (par exemple pour apprendre ce qu'est une variable) selon le contexte, avec toujours comme finalité d'aider l'enseignant·e dans sa classe :

- Qu'est-ce que l'informatique ?
- Que signifie connaître l'informatique ?
- Comment apprend-on l'informatique ?
- Comment les enseignant·e·s enseignent-illes et évaluent-illes les compétences en informatique ?
- Comment l'identité de l'apprenant·e interagit-elle avec l'apprentissage de l'informatique ?
- Comment les apprenant·e·s peuvent-illes apprendre l'informatique plus efficacement ?
- Comment les enseignant·e·s peuvent-illes enseigner l'informatique plus efficacement ?
- Comment améliorer l'accès à l'enseignement de l'informatique dans tous les contextes ?
- Comment l'enseignement de l'informatique peut-il être dispensé équitablement à tous ?
- Comment réinventer l'enseignement de l'informatique pour servir des objectifs autres que l'adéquation à une société de profit ?
- Comment les systèmes d'oppression tels que le racisme, le sexisme et le [capacitisme](https://fr.wikipedia.org/wiki/Capacitisme) façonne l'apprentissage, l'enseignement et les programmes ?
Comment pouvons-nous mettre en œuvre une éducation à l'informatique qui dépasse ces aspects ?







