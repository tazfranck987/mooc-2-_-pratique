
https://pixees.fr/podcast/pod-nsi-le-lancement/ 

On doit avoir pour chaque pilier (les trucs du CAFFA) deux activités moocable

une meme activité se retrouve dans plusieurs modules sous differents angles
mêmes documents mais pas les mêmes discussions

# 1.Penser concevoir élaborer

## activité 1 : turtle (Git - attention actuellement vide à retrouver -> envoyé par mail)
## Castor informatique (castor informatique - cf fin de doc)
## Activité charles base de données (activité1.md - Git) - déja dans le GIT
## activité2 : (dégager activités pas à pas, car idem que turtle) - activité modélisation d'un graphe

# 2.Mettre en oeuvre Animer
## Activité 1 : Dijkstra (faire le point sur les vidéos ? - une vidéo diapo - une vidéo intention - une vidéo devant tableau blanc -- actuellement c dans le Git en 1_Penser-Concevoir-Elaborer - activité 2)- renvoyer les vidéos (voir Thierry pour du copier coller)
## activité Charles Base de données (activité1.md - ) On peut aussi la trouver dans  #Penser concevoir élaborer
## activité thierry (activité2.md) graphe valué
## activité html , css, javascript (envoyer hier par mail)

# 3.Accompagner

## activité d'introduction sur la numérisation et le codage d'images (reprend le travail de Charles html, du 2...)
- Accompagnement collaboratif / 
Lien ISO (Informatique Sans Ordinateur) en faire la pub - lien vers Clermont - Lien Duflot voir Charles (Nancy) - Martin Quinson
https://pixees.fr/dans-la-famille-activites-debranchees-je-demande-les-tutos-videos-de-marie-duflot/

## Gérer l'hétérogénéîté (cf. fin du docdans le doc en bas) )
- pile : construire l'implémentation d'une liste itérative, l'interface étant donné
- en utilisant les méthodes des list pythons
- en utilisant le tableau sur dimensionné
- par chainage

## Notebook Charles gestion de l'hétérogénéité par la présentation différente mais le même but
liste python (plutôt un catalogue...)

##activité  projets Charles isola-Morpion/?nombres-simon -> fait le lien avec eval
ce qui est intéressant c'est la discussion avec les élèves : discussion en face à face (sur la structuration de données)
suivi de l'élève, cahier avec taches à réaliser aujourd'hui, plus tard...

Liens intéressants  à regarder pour questionner :
##Bruno Mermet Notion de projets https://mermet.users.greyc.fr/Enseignement/EnseignementInformatiqueLycee/Havre/Didactique/projetInformatique.html

##Apprentissage collaboratif : guide pratique
https://knowledgeone.ca/apprentissage-collaboratif-guide-pratique/?lang=fr

## Nuit du code pour promouvoir NSI dès le collège - un samedi pendant 6H - gameplay déjà fait, ils doivent programmer un jeu - en général
--> à présenter, gérer par le lycée de Tokyo
se fait sur Scratch et python

## Ne pas diffuser !  Trophée NSI : journée nationale qui aura lieu en mai : élèves 1ere et terminale NSI
concours académique national : A ne pas diffuser pour l'instant
qui se passera au mois de mai : pour 1ere et terminale NSI --> trophé NSI
pour promouvoir NSI
## pointer concours général
## france ioi - alkindi: 
    

# 4.Évaluer

## Évaluer un projet - comment on évalue - pour qui, pourquoi grille d'évaluation / D'auto évaluation...
construction grille d'évaluation - fait le lien avec le projet de Charles - Isola - Morpion - Nombres
colonne 1 : compétence / auto-évaluation (donner des items d'éval) / éval du prof
abstraire - généraliser
travail en équipe - répartition des taches
échange avec le prof - de l'aide vers l'autonomie
histoire de coordonnées avec Isola assez intéressant
contenu

## Évaluer une épreuve pratique terminale (à voir si on ajoute)
Est ce qu'on peut en prendre une, qui fonctionne bien (attention beaucoup d'erreurs ds les annales - correction sur le site de David)?
En attente des resultats du travail d'une commission C3I... Donner une épreuve pratique qui marche bien
Tres formative pour les enseignants
Activité demandée : expliciter les questions qui pourraient être poser sur un tel sujet?
Ce qui peu être intéressant : pas la même posture le jour d'une épreuve de bac ou on est "examinateur" et non formateur
Mise en avant de l'importance d'une grille d'évaluation

## devoir de Charles un peu-beaucoup-à la folie - pas du tout innovant mais grave :-))))


Liens ressources :
Evaluation formative et sommative
https://www.enseigner.ulaval.ca/ressources-pedagogiques/l-evaluation-formative-et-sommative

10 Biais de correction
https://sciencescoetpedago.wordpress.com/2021/05/17/10-biais-de-correction-a-connaitre-absolument-lorsquon-est-enseignante/amp/?__twitter_impression=true

Bernard Lahire : vidéo


Castor informatique
                                   
# Onglet 1

Dans cette partie nous allons réfléchir à la façon d'utiliser les exercices proposés par le concours "Castor Informatique" pour approcher une notion, remédier à un concept, analyser une situation pour déterminer le concept informatique sous-jacent.
lien : CS-unplugged, 
lien : ISO  marie-duflot

Le travail que vous sera demandé sera de choisir cinq exerices du castor informatique. Pour chacun de ces exercices vous devrez les décrire avec trois mots.

https://castor-informatique.fr/home.php > s'entrainer sur les sujets passés

Exemple : 
Année : 2014 
titre : attraper le monstre
mot1 : dichotomie
mot2 : Première NSI
mot3 : activité approche




# Onglet 2

menu déroulant Année (pré remplir)
menu déroulant titre (s'automatise en proposant le choix des titres une fois l'année connue)
zone de saisie 1
zone de saisie 2
zone de saisie 3

# Onglet 3
Nuage de mot qui s'affiche sur l'exercice pour comparaison avec d'autres

# 8 autres onglets comme ça

# Onglet final
Nuage de mots de tous les exercices ?




Gérer l'hétérogénéîté

                                   
Gérer l'hétérogénéîté ne signifie pas individualiser. On peut ainsi proposer une progressivité des apprentissages différentes mais dont le but est le même.
On présente ici un exemple permettant de travailler la partie du programme de terminale NSI traitant la notion de structure de données abstraites et de leurs implémentations possibles.

# Temps 1 travail avec l'interface d'une structure de données abstraites : l'exemple de la Pile

Positionnement dans la progression annuelle : 
Le travail qui est présenté s'inscrit dans le choix (bién évidemment discutable) de la progressivité suivante : 
- Pile
- File
- Liste / interface itérative
- Liste / interface récursive
- Arbre
- Graphe
La POO a déjà été introduite préalablement.

C'est donc la première fois que les élèves vont rencontrer de manière très explicite la distinction entre interface et implémentation.
Ce travail a bien sûr été préparé en première avec les p-uplet et les tableaux non dynamiques (implémentés en général avec une "list" en Python) ainsi qu'avec les p-uplet nommés et les dictionnaires (implémentés en général avec un dict en "Python")

La séquence comment avec un corpus d'activité sur les piles : 
A joindre fichier d'activité sur les piles

Ce premier temps voit la manipulation de l'interface des piles "papier-crayon". Les élèves sont incités à s'en emparer, la manipuler en écrivant leur script sur feuille blanche. Ils ne sont ainsi pas en échec devant des problèmes de syntaxe Python : l'objectif étant de prendre en main une interface et de créer le questionnement "Comment on fait en Python, les fonctions de l'interface n'existent pas en Python". Les élèves avancent ainsi à leur rythme, le but étant qu'ils se saisissent de la structure fondamentale permettant de traiter les piles (que l'on retrouvera plus tard avec les Files) qui est le "Tant que la Pile n'est pas vide, je traite la tête".

# Temps 2 Implémentation de la structure de données abstraites en Python

Après avoir passé deux à quatre heures à manipuler cette nouvelle structure abstraite, les élèves sont invités à passer sur machine pour implémenter les fonctions de l'interface des piles.
C'est dans ce temps que la gestion de l'hétérogénéité prend tout son sens. En effet, il existe principalement deux moyens : 
- implémentation par un tableau sur-dimensionné
- implémentation par un principe de chaînage.
Ces deux concepts seront retrouvés toute l'année, il n'est ainsi pas obligatoire que tous les élèves les implémentent pour les Piles.
Le questionnnement de "la meilleur implémentation" est construit tout au long de l'année en interrogeant la complexité en opération des fonctions construites en Python.

Activité 1 : Implémentaiton avec la list Python - image mentale contigue des données
A joindre

Activité 2 : Implémentation avec un tableau surdimenssionné - image mentale contigue des données
A joindre x

Activité 3 : Implémentation avec le principe de Chainage - image mentale éparpillé des données
A joindre


Typologie de l'erreur
https://fr.calameo.com/read/000302261601a643fdad4

Différents modèles d'enseignement: en présentiel, à distance, comodal et hybride
https://www.profweb.ca/publications/articles/differents-modeles-d-enseignement-en-presentiel-a-distance-comodal-et-hybride
