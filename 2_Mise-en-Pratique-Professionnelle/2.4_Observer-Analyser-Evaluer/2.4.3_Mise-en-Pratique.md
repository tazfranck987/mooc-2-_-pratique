# À faire vous-même

Les objectifs de ce Module 4 sont :

1. Proposer une ou plusieurs ressources parmi :
    - un ensemble d'évaluations (courte en classe, devoir maison etc.) sur une ou plusieurs activités vues précédemment
    - une évaluation type _Bac blanc_ sur une partie conséquente du programme
    - une grille d'évaluation pour un projet
    - une présentation et des indicateurs pour l'institution et la famille
2. Déposer vos ressources sur votre espace gitlab
3. Se rendre sur le Forum pour y mettre un _post_, dans la rubrique dédiée, pour annoncer le travail fait. D'autres pourront alors accéder à votre travail, le commenter, vous faire des retours
4. De votre côté, regardez ce que d'autres ont proposé, commentez, échangez avec les auteur-es.
